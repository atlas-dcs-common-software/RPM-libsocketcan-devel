%define PREFIX /usr/local

Name: libsocketcan-devel
Version: 0.0.9
Release: p0.cc7
Source0: libsocketcan-0.0.9.tar
Vendor: atlas-dcs@cern.ch
Summary: libsocketcan headers and libs
License: LGPL
BuildRoot: %(echo $PWD)/BUILDROOT/%{name}-%{version}
Prefix: %{PREFIX}

%description
libsocketcan headeres and libs

%prep

%setup -n libsocketcan-0.0.9

%build
gcc -c -fPIC -O2 src/libsocketcan.c -Iinclude
ar cru libsocketcan.a libsocketcan.o


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{PREFIX}/lib
cp libsocketcan.a $RPM_BUILD_ROOT/%{PREFIX}/lib
mkdir -p $RPM_BUILD_ROOT/%{PREFIX}/include
cd include
cp can_netlink.h libsocketcan.h $RPM_BUILD_ROOT/%{PREFIX}/include

#make install DESTDIR=%{buildroot}

%post
ldconfig

%postun
ldconfig

%files
%defattr(-,root,root)
%{PREFIX}/include/can_netlink.h
%{PREFIX}/include/libsocketcan.h
%{PREFIX}/lib/libsocketcan.a
